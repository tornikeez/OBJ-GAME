public class Chess extends Game {
    private Player currentPlayer;
    private boolean isBoardSetUp;

    public Chess() {
        super("Chess");
         this.currentPlayer = null;
    }

    public boolean setUpBoard() {
        System.out.println("Setting up the chess board");

        isBoardSetUp = true;
        return isBoardSetUp;
    }

    public void FinishedSetUpBoarding(){
        if(setUpBoard()){
            System.out.println("Chess board is ready!");
        } else {
            System.out.println("At first you should set up the chess board");
        }
    }

    public void startGame(Player player1, Player player2) {
        System.out.println("Starting the chess game");
        currentPlayer = player1;
        player1.setChessGame(this);
        player2.setChessGame(this);
    }

    public void switchPlayer() {
        Player player1 = null;
        if (currentPlayer == player1) {
            Player player2 = null;
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }

        System.out.println("Switching to second player");
    }

    public void finishGame(){
        System.out.println("Game finished");
    }

}
