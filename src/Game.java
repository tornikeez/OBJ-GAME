public abstract class Game {
    private String gameName;

    public Game(String gameName){
        this.gameName=gameName;
    }

    public void play(){
        System.out.println("Playing " + gameName + ".");
    }

}
