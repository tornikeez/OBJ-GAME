public class Main {
    public static void main(String[] args) {
        Chess chessGame = new Chess();

        Player player1=new Player("Alice");
        Player player2=new Player("Bob");

        chessGame.setUpBoard();
        chessGame.startGame(player1,player2);

        player1.makeMove();
        chessGame.switchPlayer();

        player2.makeMove();
        chessGame.switchPlayer();

        player1.movePiece();;
        player1.Win();

        chessGame.finishGame();





    }
}