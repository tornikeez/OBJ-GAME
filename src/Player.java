public class Player {
    private String username;
    private Chess chessGame;

    public Player(String username){
        this.username=username;
        this.chessGame=null;
    }

    public void setChessGame(Chess chessGame) {
        this.chessGame = chessGame;
    }

    public void makeMove(){
        System.out.println(username + " is making a move");
    }

    public void movePiece(){
        System.out.println("Moving a chess piece");
    }

    public void Win(){
        System.out.println(username + " Wins a game");
    }

    public void Lost(){
        System.out.println(username + " lost a game");
    }

}
